import '../assets/tailwind.scss';
import Link from 'next/link';

import PageTitle from '../components/PageTitle';

const ItemPage = () => {
	return (
		<section className="flex flex-col items-center my-12 mx-8 max-w-2xl xl:mx-auto">
			<picture>
				{/* agregar  */}
				<img src="https://dummyimage.com/288x360/000/fff.jpg" alt="" />
			</picture>
			<div className="w-full flex items-center flex-col px-4 pt-4 uppercase">
				<p className="font-bold text-md text-justify">Marca</p>	
				<h3 className="mt-4 text-md">Nombre</h3>
				<p className="mt-4 text-md text-justify">¢ 25.000</p>	
			</div>
			<button className="mx-auto my-4 hover:bg-yellow-800 bg-yellow-600 text-white font-bold py-2 px-4 rounded-full">Agregar al carrito</button>				
			<div className="w-full">
				<h3 className="self-start">Descripcion</h3>
				<p className="mt-4 text-sm text-justify">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Enim neque repudiandae ab sapiente doloribus ipsa? Doloribus dolorum vitae, molestiae, quia explicabo cupiditate dolorem, odio earum quam nihil ut hic dolores pariatur minima iusto dicta doloremque totam itaque fugiat iure sit temporibus ducimus! Necessitatibus explicabo architecto quod, aut quam placeat et eveniet amet nulla. Perspiciatis odio veniam accusamus quasi praesentium alias.</p>	
			</div>
		</section>
	);
};

export default ItemPage;
