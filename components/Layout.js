import '../assets/tailwind.scss';

import Nav from '../components/Nav';
import Footer from '../components/Footer';
const jwt = require("jsonwebtoken");

export default class Layout extends React.Component {
     state = {
          id: '',
          nombre: '',
          apellido: '',
          email: '',
          admin: ''
     }

     componentDidMount = () => {
          let userData = localStorage.getItem('sf');
          if (userData != null) {
               userData = jwt.decode(userData);
               this.state.id = userData.id;
               this.state.nombre = userData.nombre;
               this.state.apellido = userData.apellido;
               this.state.email = userData.email;
               this.state.admin = userData.admin;
               this.setState({ state: this.state });
          }
     }
     render() {
          return (
               <React.Fragment>
                    <Nav userName={this.state.nombre} userApellido={this.state.apellido} />
                    {this.props.children}
                    <Footer />
               </React.Fragment>
          )
     }
}