import '../assets/tailwind.scss';

const PageTitle = (props) => {
     return(
          <React.Fragment>
               <h3 className="text-center mx-auto my-10 uppercase text-3xl w-1/2">{props.title}</h3>
               <style jsx>{`
                    h3{
                         font-family: 'Vidaloka', sans-serif;
                    }
               `}</style>
          </React.Fragment>
     )
}

export default PageTitle;