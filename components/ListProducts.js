import '../assets/tailwind.scss';
import Item from '../components/Item';

const ListItems = (props) => {
     return (
          <section className="grid mx-4 md:mx-16 xl:mx-auto max-w-6xl">
               <Item name="Item1" imgS="https://dummyimage.com/200x248/000/fff.jpg" imgL='https://dummyimage.com/864x1080/000/fff.jpg' price="3.000" />
               <Item name="Item2" imgS="https://dummyimage.com/200x248/000/fff.jpg" imgL='https://dummyimage.com/864x1080/000/fff.jpg' price="2.000" />
               <Item name="Item3" imgS="https://dummyimage.com/200x248/000/fff.jpg" imgL='https://dummyimage.com/864x1080/000/fff.jpg' price="25.000" />
               <Item name="Item4" imgS="https://dummyimage.com/200x248/000/fff.jpg" imgL='https://dummyimage.com/864x1080/000/fff.jpg' price="30.000" />
          </section>
     );
};

export default ListItems;