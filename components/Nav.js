import '../assets/tailwind.scss';
import Link from 'next/link';

const show = () => {
     if (document.getElementById('nav').className.includes('hidden')) {
          document.getElementById('nav').classList.remove('hidden');
     } else {
          document.getElementById('nav').classList.add('hidden');
     }
}
const Nav = (props) => {
     return (
          <React.Fragment>
               <nav className="border-b-4 border-yellow-600 h-24 shadow-md ">
                    <div className="flex justify-between px-4 h-full max1200 mx-auto">
                         <Link href="/">
                              <svg className="h-10 flex items-start cursor-pointer my-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 274.448 58.071"><path d="M21.411 2.607a4.127 4.127 0 0 0 .736-1.118q.425-.894.963-.894.765 0 .935 1.246l.991 7.76.113.935a.676.676 0 0 1-.765.765 1.086 1.086 0 0 1-.821-.27 9.638 9.638 0 0 1-.963-1.747 12.22 12.22 0 0 0-4.021-4.546 9.6 9.6 0 0 0-5.636-2.024A6.753 6.753 0 0 0 8.369 4.29a5.013 5.013 0 0 0-1.827 3.962 4.555 4.555 0 0 0 1.7 3.436q1.7 1.533 6.542 2.895 7.59 2.156 10.28 5.327a10.7 10.7 0 0 1 2.662 7.05 11.018 11.018 0 0 1-1.954 6.654 14.747 14.747 0 0 1-4.669 4.237 14 14 0 0 1-6.971 1.543 19.146 19.146 0 0 1-4.984-.694 28 28 0 0 1-5.324-2.086 5.574 5.574 0 0 0-.963 1.7q-.458 1.165-1.133 1.165-.708 0-.85-1.529L.057 27.67Q0 26.99 0 26.82a.739.739 0 0 1 .85-.821q.623 0 1.189 1.416a20.04 20.04 0 0 0 4.7 7.08 9.615 9.615 0 0 0 6.884 2.831 7.44 7.44 0 0 0 5.4-1.982 6.11 6.11 0 0 0 2.053-4.5 5.521 5.521 0 0 0-1.9-4.248q-1.9-1.728-6.825-3.313a35.915 35.915 0 0 1-7.123-2.974 10.51 10.51 0 0 1-3.741-3.895 10.107 10.107 0 0 1-1.4-5.027 9.828 9.828 0 0 1 3.441-7.562Q6.969.71 12.888.71a27.025 27.025 0 0 1 8.523 1.897zm37.156 23.307a13.015 13.015 0 0 1-3.88 9.261 13.061 13.061 0 0 1-9.855 4.049 13.217 13.217 0 0 1-9.232-3.4 11.652 11.652 0 0 1-3.738-9.062 11.921 11.921 0 0 1 3.951-9.3 13.863 13.863 0 0 1 9.615-3.554 13.51 13.51 0 0 1 9.445 3.356 11.154 11.154 0 0 1 3.694 8.65zm-7.816 1.942a21.941 21.941 0 0 0-1.548-8.409q-1.543-3.757-4.432-3.757a4.3 4.3 0 0 0-3.933 2.624 15.964 15.964 0 0 0-1.388 7.359 19.749 19.749 0 0 0 1.572 8.465q1.572 3.3 4.319 3.3a4.34 4.34 0 0 0 4.05-2.637 15.2 15.2 0 0 0 1.359-6.946zM72.784 0l.085 23.761q.028 11.385.595 12.433a2.49 2.49 0 0 0 2.266 1.133q1.982.085 1.982.793 0 .878-1.218.878-.34 0-.991-.057-2.748-.227-5.325-.227-4.191 0-6.2.22a9.334 9.334 0 0 1-1.02.063q-1.274 0-1.274-.765 0-.736 1.444-.821a4.67 4.67 0 0 0 2.275-.567 1.975 1.975 0 0 0 .878-1.346 30.714 30.714 0 0 0 .156-4.039l-.113-11.139.085-8.843q0-5.5-.524-6.675t-3.1-1.6q-1.558-.227-1.558-.906a.577.577 0 0 1 .356-.582 7.158 7.158 0 0 1 1.742-.269A56.609 56.609 0 0 0 72.784 0zm15.6 25.8q-.028.821-.028 1.189a11.513 11.513 0 0 0 1.968 6.811 6.176 6.176 0 0 0 5.31 2.79 10.1 10.1 0 0 0 6.74-2.974q.68-.595.963-.595.736 0 .736.793 0 .623-1.7 2.237a10.991 10.991 0 0 1-4.163 2.436 16.014 16.014 0 0 1-5.1.821q-5.777 0-9.048-3.526a11.819 11.819 0 0 1-3.259-8.311 13.356 13.356 0 0 1 3.752-9.643 12.466 12.466 0 0 1 9.36-3.894q5.268 0 8.029 3.54a11.146 11.146 0 0 1 2.759 6.74q0 1.02-.538 1.218a16.1 16.1 0 0 1-3.483.2zm.17-2.067q4.191.057 4.843.057 3.2 0 3.922-.453a2.016 2.016 0 0 0 .722-1.841 6.684 6.684 0 0 0-1.331-4.106 3.883 3.883 0 0 0-3.172-1.784q-4.357-.001-4.98 8.127zM97.186.142a2.365 2.365 0 0 1 1.7.722 2.227 2.227 0 0 1 .738 1.628q0 1.728-2.981 3.908-2.328 1.7-4.4 3.4t-2.754 1.7a1.127 1.127 0 0 1-.724-.34.926.926 0 0 1-.383-.651 28.369 28.369 0 0 1 2.547-4.095 39.339 39.339 0 0 1 3.774-5.026A3.557 3.557 0 0 1 97.19.142zM118.664 38.7q-3.886 12.942-3.886 14.7a2.688 2.688 0 0 0 .765 1.713 4.535 4.535 0 0 0 2.634 1.26 6.4 6.4 0 0 1 1.1.255q.17.085.17.4 0 .51-.793.51-.566 0-1.473-.085-5.522-.4-9.4-.4l-3.736.082q-.85 0-.85-.581 0-.469 1.5-.551 3.512-.256 4.475-2.833t4.291-14.145q3.328-11.569 3.328-14.627a2.191 2.191 0 0 0-1.147-1.954 4.977 4.977 0 0 0-2.563-.736q-.991 0-.991-.51 0-.651.935-.651l18.578.283q3.71 0 6.287.113 2.266.113 2.889.113t1.982-.085q.057 1.1.057 1.614 0 9.034-.878 9.034-.623 0-.765-1.048a13.368 13.368 0 0 0-1.43-4.347 9.287 9.287 0 0 0-2.082-2.9 6.033 6.033 0 0 0-2.8-1.062 53.909 53.909 0 0 0-7.08-.3 17.418 17.418 0 0 0-3.795.312q-1.7 2.549-4.814 15.151 1.388.028 2.549.028 5.267 0 6.966-1.1a11.566 11.566 0 0 0 3.427-4.56q.34-.68.623-.68.51 0 .51.651a1.674 1.674 0 0 1-.113.566q-.595 1.529-3.71 14.33-.255.963-.708.963a.451.451 0 0 1-.51-.51l.028-.17a13.864 13.864 0 0 0 .283-2.719q0-5.579-7.84-5.579-.683-.001-2.013.055zm42.269-5.1q-.793 1.756-3.2 9.4l-2.492 7.9a10.977 10.977 0 0 0-.793 3.682.839.839 0 0 0 .935.963 3.868 3.868 0 0 0 1.914-.92 14.085 14.085 0 0 0 2.456-2.331q.794-.935 1.106-.935a.426.426 0 0 1 .482.481q0 .765-3.089 3.384t-5.499 2.621q-1.644 0-1.644-2.232 0-1.553.2-3.954-4.394 5.931-9.181 5.931a3.4 3.4 0 0 1-3.058-1.515 6.1 6.1 0 0 1-.991-3.413q0-5.466 5.452-12.56t10.294-7.094a2.525 2.525 0 0 1 2.025.88 4.9 4.9 0 0 1 .949 2.667 35.475 35.475 0 0 1 1.614-3.406q.906.142 1.728.283.594.142.792.171zm-8.694.963q-1.643 0-4.021 2.368a20.534 20.534 0 0 0-4.22 6.962 21.519 21.519 0 0 0-1.845 7.66 4.739 4.739 0 0 0 .651 2.736 2.206 2.206 0 0 0 1.926.922q3.285 0 6.641-6.018t3.356-10.889a4.815 4.815 0 0 0-.694-2.662 2.074 2.074 0 0 0-1.794-1.077zm25.601 15.272a7.94 7.94 0 0 1-2.407 5.772 7.707 7.707 0 0 1-5.664 2.436 6.078 6.078 0 0 1-4.375-1.5 4.133 4.133 0 0 1-1.572-2.8 2.63 2.63 0 0 1 .623-1.643 1.987 1.987 0 0 1 1.643-.793 1.981 1.981 0 0 1 1.487.566 1.878 1.878 0 0 1 .552 1.359 2.882 2.882 0 0 1-.255 1.048 3.011 3.011 0 0 0-.255.963 1.228 1.228 0 0 0 .636 1.115 3.791 3.791 0 0 0 1.912.382 4.4 4.4 0 0 0 3.3-1.2 4.038 4.038 0 0 0 1.175-2.931q0-1.7-2.237-4.7-3.6-4.843-3.6-7.816a7.252 7.252 0 0 1 2.011-4.9 6.339 6.339 0 0 1 4.928-2.266 5.961 5.961 0 0 1 3.639 1.161 3.229 3.229 0 0 1 1.572 2.547 2.252 2.252 0 0 1-.609 1.572 1.881 1.881 0 0 1-1.43.666q-1.586 0-1.586-1.671l.028-1.529a1.285 1.285 0 0 0-.736-1.076 3.224 3.224 0 0 0-1.586-.425 3.751 3.751 0 0 0-2.379.878 3.063 3.063 0 0 0-1.102 2.546q0 1.728 3.2 5.777 3.087 3.908 3.087 6.462zm12.744-7.7a54.1 54.1 0 0 1 6.641-7.349 7.116 7.116 0 0 1 4.291-2.025 3.086 3.086 0 0 1 2.322.906 3.091 3.091 0 0 1 .878 2.237q0 2.351-2.832 10.337-2.631 7.384-2.631 8.828 0 .906.822.906a4.631 4.631 0 0 0 2.408-1.44 21.006 21.006 0 0 0 2.977-3.313q.453-.595.793-.595.481 0 .481.595 0 .906-3.018 3.852t-5.71 2.941q-2.777 0-2.777-2.039a10.443 10.443 0 0 1 .85-3.625q1.388-3.483 3.115-8.921a31.792 31.792 0 0 0 1.728-7.023 1.646 1.646 0 0 0-.227-.963.817.817 0 0 0-.708-.312q-1.3 0-4.163 2.492a21.236 21.236 0 0 0-4.715 5.891 54.071 54.071 0 0 0-3.756 9.487q-1.186 3.767-1.427 4.347t-.581.581a7.929 7.929 0 0 1-2.011-.312q-1.161-.311-1.161-.51a1.848 1.848 0 0 1 .142-.651q.283-.821 4.22-12.886l4.018-12.546 2.1-7.39a5.587 5.587 0 0 0 .368-1.77q0-.942-3.087-1.513-1.076-.2-1.076-.724t1.133-.524l4.9.028a17.846 17.846 0 0 1 2.605.113.272.272 0 0 1 .17.255l-.113.4a3.436 3.436 0 0 0-.17.595q-.738 3.394-6.799 21.632zm32.682-22.826a2.119 2.119 0 0 1 1.558.656 2.146 2.146 0 0 1 .651 1.568 2.179 2.179 0 0 1-.651 1.6 2.119 2.119 0 0 1-1.558.656 2.151 2.151 0 0 1-1.586-.656 2.179 2.179 0 0 1-.651-1.6 2.11 2.11 0 0 1 .666-1.582 2.19 2.19 0 0 1 1.571-.65zm-9.176 36.298a1.063 1.063 0 0 0 .213.722.77.77 0 0 0 .61.241 5.13 5.13 0 0 0 2.438-.991 16.242 16.242 0 0 0 2.977-2.464q.623-.651.992-.651.51 0 .51.453 0 .991-2.862 3.044a9.3 9.3 0 0 1-5.3 2.053 3.759 3.759 0 0 1-2.366-.637 1.861 1.861 0 0 1-.808-1.487q0-1.274 3.144-8.921 3.993-9.629 3.993-10.932a1.235 1.235 0 0 0-.184-.75.685.685 0 0 0-.581-.241q-1.22 0-3.913 2.605-1.049 1.02-1.474 1.02-.4 0-.4-.453 0-.736 2.976-3.016a9.145 9.145 0 0 1 5.3-2.28 2.22 2.22 0 0 1 1.658.722 2.2 2.2 0 0 1 .694 1.543 5.939 5.939 0 0 1-.354 2.011q-.354.991-4.234 10.2-3.029 7.728-3.029 8.209zm24.242-22.4a6.982 6.982 0 0 1 5.919 2.882 11.062 11.062 0 0 1 2.152 6.81 15.951 15.951 0 0 1-3.88 10.271q-3.879 4.76-9.26 4.76a6.166 6.166 0 0 1-5.721-2.91 11.639 11.639 0 0 1-1.756-6.1 16.708 16.708 0 0 1 2.181-8.166 15.148 15.148 0 0 1 5.069-5.75 10.23 10.23 0 0 1 5.297-1.798zm.538 1.189a5.376 5.376 0 0 0-3.5 1.834q-1.912 1.834-3.767 6.6a24.743 24.743 0 0 0-1.85 8.945 6.373 6.373 0 0 0 .935 3.5 2.863 2.863 0 0 0 2.521 1.467q1.784 0 3.866-2.116a19.736 19.736 0 0 0 3.837-6.546 22.831 22.831 0 0 0 1.756-8.407 6.769 6.769 0 0 0-.949-3.964 3.283 3.283 0 0 0-2.848-1.313zm20.532 8.723a36.171 36.171 0 0 1 5.636-7.519q2.634-2.506 4.9-2.506a2.39 2.39 0 0 1 2.039.949 3.659 3.659 0 0 1 .708 2.251 12.624 12.624 0 0 1-.467 3.328q-.465 1.713-2.337 6.923-2.521 7-2.521 8.241 0 .935.822.935a4.567 4.567 0 0 0 2.283-1.26 10.031 10.031 0 0 0 2.288-2.479q.736-1.274 1.218-1.274t.481.566q0 1.161-2.9 3.979t-5.577 2.818q-2.607 0-2.607-2.181 0-2.266 4.163-13.141a19.641 19.641 0 0 0 1.614-6.287 1.638 1.638 0 0 0-.241-.977.872.872 0 0 0-.75-.326q-.85 0-3.186 2.167a30.874 30.874 0 0 0-5.31 7.689 109.244 109.244 0 0 0-4.871 10.139q-1.161 2.861-1.534 2.861-2.775 0-2.775-.736l.085-.283 4.79-13.849q1.869-5.069 1.869-7.023 0-.85-.794-.85a4.637 4.637 0 0 0-2.3 1.4 18.292 18.292 0 0 0-2.524 2.648q-.736.963-1.189.963-.425 0-.425-.453 0-.906 3.075-3.852t5.909-2.945a1.622 1.622 0 0 1 1.389.651 2.255 2.255 0 0 1 .482 1.359 73.136 73.136 0 0 1-1.442 8.073z" fill="#b69d47" /></svg>
                         </Link>
                         <div className="flex items-center justify-between">
                              <ul className="md:flex hidden">
                                   <li className="mx-4">
                                        <Link href="/tienda" prefetch>
                                             <a className="uppercase border-b border-transparent hover:border-yellow-600 pb-2 text-xs xl:text-base">Blusas</a>
                                        </Link>
                                   </li>
                                   <li className="mx-4">
                                        <Link href="/tienda" prefetch>
                                             <a className="uppercase border-b border-transparent hover:border-yellow-600 pb-2 text-xs xl:text-base">Vestidos</a>
                                        </Link>
                                   </li>
                                   <li className="mx-4">
                                        <Link href="/tienda?categoria=enterizos" as="/tienda/enterizos" prefetch>
                                             <a className="uppercase border-b border-transparent hover:border-yellow-600 pb-2 text-xs xl:text-base">Enterizos</a>
                                        </Link>
                                   </li>
                                   <li className="mx-4">
                                        <Link href="/tienda" prefetch>
                                             <a className="uppercase border-b border-transparent hover:border-yellow-600 pb-2 text-xs xl:text-base">Colas</a>
                                        </Link>
                                   </li>
                                   <li className="mx-4">
                                        <Link href="/tienda" prefetch>
                                             <a className="uppercase border-b border-transparent hover:border-yellow-600 pb-2 text-xs xl:text-base">Abrigos</a>
                                        </Link>
                                   </li>
                              </ul>
                              <Link href="/carrito">
                                   <span className="bg-yellow-600 h-6 w-8 h-6 xl:w-10 xl:h-8 xl:text-lg rounded-full text-center text-white flex justify-center items-center hover:bg-yellow-700 cartItems cursor-pointer mx-8">0</span>
                              </Link>
                              <svg className="h-6 md:hidden hover:text-gray fillGray" onClick={show} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" /></svg>
                              {
                                   props.userName != '' ? (
                                        <Link href={`/perfil?nombre=${props.userName}&apellido=${props.userApellido}`} as="/perfil">
                                             <a className="hidden md:flex items-center justify-around mx-12 p-2 rounded hover:bg-gray-200">
                                                  <img className="h-8 w-8 rounded-full" src="https://randomuser.me/api/portraits/women/17.jpg" alt="Imagen" />
                                                  <p className="uppercase ml-6 text-xs lg:text-sm">{`${props.userName} ${props.userApellido}`}</p>
                                             </a>
                                        </Link>
                                   )
                                        :
                                        <Link href="/iniciar-sesion ">
                                             <a className="hidden md:flex items-center justify-around py-4 px-2 rounded hover:bg-gray-200 text-xs xl:text-base">
                                                  Iniciar Sesión / Registrarse
                                   </a>
                                        </Link>
                              }
                         </div>
                    </div>
               </nav>
               {/* desktop */}
               <section className="hidden bg-white border-b-4 border-yellow-600 py-6 shadow-lg w-full z-10 static" id="nav">
                    {
                         props.userName != '' ? (
                              <Link href={{ pathname: '/perfil', query: { nombre: `${props.userName}` } }}>
                                   <a className=" flex items-center justify-around mx-12 py-4 px-2 rounded hover:bg-gray-200">
                                        <img className="h-16 w-16 rounded-full" src="https://randomuser.me/api/portraits/women/17.jpg" alt="Imagen" />
                                        <p className="uppercase">{`${props.userName} ${props.userApellido}`}</p>
                                   </a>
                              </Link>
                         )
                              :
                              <Link href="/iniciar-sesion">
                                   <a className="flex items-center justify-around mx-12 py-4 px-2 rounded hover:bg-gray-200">
                                        Iniciar Sesión / Registrarse
                                        </a>
                              </Link>
                    }
                    <ul className="mx-8 mt-4 border-t-2 border-yellow-600">
                         <li className="my-6 uppercase">Qué gustas vestir hoy?</li>
                         <li className="ml-6 my-6">
                              <Link href="/tienda" prefetch>
                                   <a className="uppercase border-b border-transparent hover:border-yellow-600 pb-2 text-s xl:text-base">Blusas</a>
                              </Link>
                         </li>
                         <li className="ml-6 my-6">
                              <Link href="/tienda" prefetch>
                                   <a className="uppercase border-b border-transparent hover:border-yellow-600 pb-2 text-s xl:text-base">Vestidos</a>
                              </Link>
                         </li>
                         <li className="ml-6 my-6">
                              <Link href="/tienda" prefetch>
                                   <a className="uppercase border-b border-transparent hover:border-yellow-600 pb-2 text-s xl:text-base">Enterizos</a>
                              </Link>
                         </li>
                         <li className="ml-6 my-6">
                              <Link href="/tienda" prefetch>
                                   <a className="uppercase border-b border-transparent hover:border-yellow-600 pb-2 text-s xl:text-base">Colas</a>
                              </Link>
                         </li>
                         <li className="ml-6 my-6">
                              <Link href="/tienda" prefetch>
                                   <a className="uppercase border-b border-transparent hover:border-yellow-600 pb-2 text-s xl:text-base">Abrigos</a>
                              </Link>
                         </li>
                    </ul>
               </section>
               <div classlist="md:flex bg-gray-200 h-4">
                    <ul className="hidden mx-8 mt-4 border-t-2 border-yellow-600">
                         <li className="ml-6 my-6">
                              <Link href="/tienda">
                                   <a className="uppercase border-b border-transparent hover:border-yellow-600 pb-2">Blusas</a>
                              </Link>
                         </li>
                         <li className="ml-6 my-6">
                              <Link href="/tienda">
                                   <a className="uppercase border-b border-transparent hover:border-yellow-600 pb-2">Vestidos</a>
                              </Link>
                         </li>
                         <li className="ml-6 my-6">
                              <Link href="/tienda">
                                   <a className="uppercase border-b border-transparent hover:border-yellow-600 pb-2">Enterizos</a>
                              </Link>
                         </li>
                         <li className="ml-6 my-6">
                              <Link href="/tienda">
                                   <a className="uppercase border-b border-transparent hover:border-yellow-600 pb-2">Colas</a>
                              </Link>
                         </li>
                         <li className="ml-6 my-6">
                              <Link href="/tienda">
                                   <a className="uppercase border-b border-transparent hover:border-yellow-600 pb-2">Abrigos</a>
                              </Link>
                         </li>
                    </ul>
               </div>
               <style jsx>{`
               span{
                    font-family: 'Vidaloka', sans-serif;
               }
          `}</style>
          </React.Fragment>
     )
}

export default Nav;