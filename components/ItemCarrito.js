import '../assets/tailwind.scss';

import Layout from '../components/Layout';
import Link from 'next/link';

export default (props) => {
	return (
		<div className="flex items-center justify-around mt-2 md:justify-between">
			<div className="flex justify-around">
				<Link href="">
					<picture>
						<source media="(min-width: 768px)" srcSet="https://dummyimage.com/152x190/000/fff.jpg"/>
						<img src="https://dummyimage.com/76x95/000/fff.jpg" alt="Flowers"/>
					</picture>
				</Link>
				<div className="ml-8 flex flex-col justify-center">
					<p className="text-sm md:text-base font-bold">Marca</p>
					<p className="text-sm md:text-base">Nombre</p>
					<p className="text-sm mt-2 md:text-base">¢ 15.000</p>
				</div>
			</div>
			<div className="flex flex-col items-center">
				<svg className="w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M505.752 358.248L271.085 123.582c-8.331-8.331-21.839-8.331-30.17 0L6.248 358.248c-8.331 8.331-8.331 21.839 0 30.17s21.839 8.331 30.17 0L256 168.837l219.582 219.582c8.331 8.331 21.839 8.331 30.17 0s8.331-21.839 0-30.171z"/></svg>
				<p className="font-bold text-xl">5</p>
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M505.752 123.582c-8.331-8.331-21.839-8.331-30.17 0L256 343.163 36.418 123.582c-8.331-8.331-21.839-8.331-30.17 0s-8.331 21.839 0 30.17l234.667 234.667c8.331 8.331 21.839 8.331 30.17 0l234.667-234.667c8.331-8.332 8.331-21.839 0-30.17z"/></svg>
			</div>
		</div>
	);
};
