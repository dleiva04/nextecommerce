const Main = () => {
	return (
		<section className="mx-2">
			<div className="w-full mt-2">
				<img
					src="https://images.unsplash.com/photo-1512436991641-6745cdb1723f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2250&q=80"
					alt=""
				/>
			</div>
		</section>
	);
};

export default Main;
