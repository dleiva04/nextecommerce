import '../assets/tailwind.scss';
import Link from 'next/link';

const Carousel = () => {
     let picture = 1;
     return(
          <React.Fragment>
               <div className="relative arrows mx-auto max-w-6xl px-2">
                    <div className="w-full flex items-center justify-between">
                         <span className="text-white hover:bg-white p-4 rounded hover:text-black">❮</span>
                         <span className="text-white hover:bg-white p-4 rounded hover:text-black">❯</span>
                    </div>
               </div>
               <section className="md:-mt-12 flex items-center flex-col mx-auto max-w-6xl">
                    <picture className="mt-2">
                         <source media="(min-width: 768px)" srcSet="https://dummyimage.com/1400x550/000/fff.jpg"/>
                         <img src="https://dummyimage.com/345x345/000/fff.jpg" alt="Flowers"/>
                    </picture>
                    <picture className="mt-2 md:hidden">
                         <source media="(min-width: 768px)" srcSet="https://dummyimage.com/1400x550/1976d2/fff.jpg"/>
                         <img src="https://dummyimage.com/345x345/000/fff.jpg" alt="Flowers"/>
                    </picture>
                    <picture className="mt-2 md:hidden">
                         <source media="(min-width: 768px)" srcSet="https://dummyimage.com/1400x550/19d24d/fff.jpg"/>
                         <img src="https://dummyimage.com/345x345/000/fff.jpg" alt="Flowers"/>
                    </picture>
               </section>
               <div className="hidden md:flex items-center justify-center mt-2">
                    <span className="dot active"></span>
                    <span className="dot"></span>
                    <span className="dot"></span>
               </div>
          </React.Fragment>
     )
}

export default Carousel;