import '../assets/tailwind.scss';
import Link from 'next/link';

const Item = (props) => {
     return (
          <Link href="/articulo">
               <div className="border-2 border-gray-100 rounded cursor-pointer hover:bg-gray-200">
                    <picture>
                         <source media="(min-width: 650px)" srcSet={props.imgL}/>
                         <img src={props.imgS} alt="Flowers"/>
                    </picture>
                    <div className="py-2 px-2 text-center">
                         <p className="text-sm md:text-base font-bold">{props.brand}</p>
                         <p className="text-sm md:text-base">{props.name}</p>
                         <p className="text-sm mt-2 md:text-base">¢{props.price}</p>
                    </div>
               </div>
          </Link>
     )
}

export default Item;