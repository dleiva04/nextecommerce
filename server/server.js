const express = require("express");
const next = require("next");
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors');
const PORT = process.env.PORT || 3000;
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();

app
  .prepare()
  .then(() => {
    const server = express();
    server.use(cors());
    dotenv.config();

    mongoose.connect(`mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@solefashiondb-n7lw4.mongodb.net/${process.env.MONGO_DB}?retryWrites=true&w=majority`, { useNewUrlParser: true })
      .then(() => {
        console.log('=> Conectado al cluster');
      })
      .catch((err) => {
        console.log("error al conectarse =>", err);
      })

    server.use(express.urlencoded({ extended: true }));
    server.use(express.json());

    // Controllers
    const UserControl = require('../controllers/UserController');

    // Routes
    server.post('/api/user/create', UserControl.create);
    server.post('/api/user/login', UserControl.login);

    server.get("*", (req, res) => {
      return handle(req, res);
    });

    server.listen(PORT, err => {
      if (err) throw err;
      console.log(`> Ready on ${PORT}`);
    });
  })
  .catch(ex => {
    console.error(ex.stack);
    process.exit(1);
  });
