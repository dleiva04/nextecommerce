import '../assets/tailwind.scss';
import axios from 'axios';
import Link from 'next/link';
import Router from 'next/router'
import Layout from '../components/Layout';
import Alert from '../components/NotificationForm';
import PageTitle from '../components/PageTitle';

export default class iniciarSesion extends React.Component {
     state = {
          email: '',
          password: '',
          validate: false,
          emailState:'',
          passwordState:'',

          mensaje:'',

          error: 'border-red-400',
          valid: 'border-green-400'
     }

     errorHandling = (param,error) =>{
          this.setState({
               param:error
          });
     }

     validate = () => {
          let show = true;
          // email
          if (this.state.email.length > 0 && this.state.email.includes('@') && this.state.email.includes('.')) {
               this.state.emailState = this.state.valid;
          } else {
               this.state.emailState = this.state.error;
               this.state.mensaje = 'Email y/o contraseña incorrecto';
               show = false;
          }
          // password
          if (this.state.password.length > 0) {
               this.state.passwordState = this.state.valid;
          } else {
               this.state.passwordState = this.state.error;
               this.state.mensaje = 'Email y/o contraseña incorrecto';
               show = false;
          }
          this.setState({ state: this.state });
          return show;
     }

     handleChange = e => {
          this.setState({
               [e.target.name]: e.target.value
          });
          if (this.state.validate) {
               this.validate();
          }
     }

     handleClick = e => {
          e.preventDefault();
          this.state.validate = true;
          if (this.validate(e)) {
               let data = {
                    email: this.state.email,
                    password: this.state.password
               }
               axios.post('/api/user/login', data)
                    .then(response => {
                         if(!response.data.success){
                              this.setState({
                                   emailState: this.state.error,
                                   mensaje: 'Usuario no registrado'
                              });
                         }else{
                              localStorage.setItem('sf',response.data.result);
                              Router.push('/');
                         }
                    })
                    .catch(error => {
                         console.log(error);
                    });
          }
     }



     handleKeyPress = e => {
          if (e.key == 'Enter') {
               this.handleClick(e);
          }
     }

     render() {
          return (
               <Layout>
                    <PageTitle title="Iniciar Sesión" />
                    <form className="flex flex-col mx-8 max-w-md md:mx-auto" onKeyPress={this.handleKeyPress}>
                         <input id="txtEmail" className={`h-12 px-4 mt-2 border-2 border-gray-200 rounded-lg focus:outline-none ${this.state.emailError}`} type="email" name="email" placeholder="Email" onChange={this.handleChange} value={this.state.email} />
                         <input id="txtPassword" className={`h-12 px-4 mt-2 border-2 border-gray-200 rounded-lg focus:outline-none ${this.state.emailError}`} type="password" name="password" placeholder="Contraseña" onChange={this.handleChange} value={this.state.password} />
                         {
                              this.state.passwordState == this.state.error || this.state.emailState == this.state.error ? (
                                   <Alert mensaje={this.state.mensaje} show={true} />
                              ) : ('')
                         }
                         <button className="mx-auto mt-6 hover:bg-yellow-800 bg-yellow-600 text-white font-bold py-2 px-4 rounded-full focus:outline-none" onClick={this.handleClick}>Iniciar Sesión</button>
                    </form>
                    <p className="text-center my-12">No tienes cuenta? <Link href="/registrarse"><a className="no-underline hover:underline text-blue-500">Regístrate</a></Link></p>
               </Layout>
          )
     }
}