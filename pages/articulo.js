import '../assets/tailwind.scss';

import Layout from '../components/Layout';
import ItemPage from '../components/ItemPage';


export default () => {
	return (
		<Layout>	
               <ItemPage/>
		</Layout>
	);
};
