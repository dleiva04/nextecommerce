import '../assets/tailwind.scss';

import Layout from '../components/Layout';
import ItemCarrito from '../components/ItemCarrito';
import PageTitle from '../components/PageTitle';

export default () => {
	return (
		<Layout>
			<PageTitle title="carrito"/>
			<div className="max-w-xl md:mx-auto">
				<ItemCarrito/>
				<ItemCarrito/>
			</div>
			<div className="border-t border-b border-yellow-900 py-4 px-8 my-6 flex justify-end max-w-xl mx-auto">
				<p className="font-bold mx-2">Total:</p>
				<p>¢ 20.000</p>
			</div>
			<div className="flex items-center">
				<button className="mx-auto my-4 hover:bg-yellow-800 bg-yellow-600 text-white font-bold py-2 px-4 rounded-full">Realizar pago</button>
			</div>
		</Layout>
	);
};
