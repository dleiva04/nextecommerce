import '../assets/tailwind.scss';
import axios from 'axios';
import Link from 'next/link';
import Router from 'next/router'
import Layout from '../components/Layout';
import Alert from '../components/NotificationForm';
import PageTitle from '../components/PageTitle';



export default class registrarse extends React.Component {

     state = {
          nombre: '',
          apellido: '',
          email: '',
          password: '',
          password2: '',
          validate: false,

          nombreState: '',
          apellidoState: '',
          emailState: '',
          emailMsg: '',
          passwordState: '',

               error: 'border-red-400',
               valid: 'border-green-400'
     }

     handleChange = e => {
          this.setState({
               [e.target.name]: e.target.value
          });
          if (this.state.validate) {
               this.validate(e);
          }
     }

     handleClick = e => {
          e.preventDefault();
          this.state.validate = true;
          if (this.validate(e)) {
               let data = {
                    nombre: this.state.nombre,
                    apellido: this.state.apellido,
                    email: this.state.email,
                    password: this.state.password
               }
               axios.post('/api/user/create', data)
                    .then(response => {
                         if (response.data.result.errmsg!= undefined) {
                              if(response.data.result.errmsg.includes('@')){
                                   console.log(this.state.email);
                                   this.setState({
                                        emailMsg: 'Email ya está registrado'
                                   })
                              }
                         }
                         localStorage.setItem('sf',response.data.result);
                         Router.push('/');
                    })
                    .catch(error => {
                         console.log(error);
                    });
          }
     }

     validate = e => {
          let show = true;
          // nombre
          if (this.state.nombre.length > 0) {
               this.state.nombreState = this.state.valid;
          } else if (this.state.nombre.match(/^[a-zA-Z]+$/)) {
               this.state.nombreState = this.state.error;
               show = false;
          } else {
               this.state.nombreState = this.state.error;
               show = false;
          }
          // apellido
          if (this.state.apellido.length > 0 && this.state.apellido.match(/^[a-zA-Z]+$/)) {
               this.state.apellidoState = this.state.valid;
          } else {
               this.state.apellidoState = this.state.error;
               show = false;
          }
          // email
          if (this.state.email.length > 0 && this.state.email.includes('@') && this.state.email.includes('.')) {
               this.state.emailState = this.state.valid;
          } else {
               this.state.emailState = this.state.error;
               this.state.emailMsg = 'Email no es válido';
               show = false;
          }
          // password
          if (this.state.password.length > 0 && this.state.password2.length > 0 && this.state.password == this.state.password2) {
               this.state.passwordState = this.state.valid;
          } else {
               this.state.passwordState = this.state.error;
               show = false;
          }
          this.setState({ state: this.state });
          return show;
     }

     handleKeyPress = e => {
          if (e.key == 'Enter') {
               this.handleClick(e);
          }
     }

     render() {
          return (
               <Layout>
                    <PageTitle title="Registrarse" />
                    <form className="flex flex-col mx-8 max-w-md md:mx-auto" onKeyPress={this.handleKeyPress}>
                         <input id="txtNombre" className={`h-12 px-4 mt-2 border-2 border-gray-200 rounded-lg focus:outline-none ${this.state.nombreState}`} type="text" name="nombre" placeholder="Nombre" onChange={this.handleChange} value={this.state.nombre} />
                         {
                              this.state.nombreState == this.state.error ? (
                                   <Alert mensaje="Caracteres no válidos en nombre" show={true} />
                              ) : ('')
                         }
                         <input id="txtApellido" className={`h-12 px-4 mt-2 border-2 border-gray-200 rounded-lg focus:outline-none ${this.state.apellidoState}`} type="text" name="apellido" placeholder="Apellido" onChange={this.handleChange} value={this.state.apellido} />
                         {
                              this.state.apellidoState == this.state.error ? (
                                   <Alert mensaje="Caracteres no válidos en apellido" show={true} />
                              ) : ('')
                         }
                         <input id="txtEmail" className={`h-12 px-4 mt-2 border-2 border-gray-200 rounded-lg focus:outline-none ${this.state.emailState}`} type="email" name="email" placeholder="Email" onChange={this.handleChange} value={this.state.email} />
                         {
                              this.state.emailState == this.state.error ? (
                                   <Alert mensaje={this.state.emailMsg} show={true} />
                              ) : ('')
                         }
                         <input id="txtPassword" className={`h-12 px-4 mt-2 border-2 border-gray-200 rounded-lg focus:outline-none ${this.state.passwordState}`} type="password" name="password" placeholder="Contraseña" onChange={this.handleChange} value={this.state.password} />
                         <input id="txtPassword2" className={`h-12 px-4 mt-2 border-2 border-gray-200 rounded-lg focus:outline-none ${this.state.passwordState}`} type="password" name="password2" placeholder="Repetir Contraseña" onChange={this.handleChange} value={this.state.password2} />
                         {
                              this.state.passwordState == this.state.error ? (
                                   <Alert mensaje="Contraseña con menos de 8 caracteres y/o no son iguales" show={true} />
                              ) : ('')
                         }
                         <button className="mx-auto mt-6 hover:bg-yellow-800 bg-yellow-600 text-white font-bold py-2 px-4 rounded-full focus:outline-none" onClick={this.handleClick} >Registrarse</button>
                    </form>
                    <p className="text-center my-12">Ya tienes cuenta? <Link href="/iniciar-sesion"><a className="no-underline hover:underline text-blue-500">Iniciar Sesión</a></Link></p>
               </Layout>
          )
     }
}