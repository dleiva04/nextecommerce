import '../assets/tailwind.scss';

import PageTitle from '../components/PageTitle';
import Layout from '../components/Layout';
import ListItems from '../components/ListProducts';



export default class Tienda extends React.Component {
	static async getInitialProps({ query }) {

		return query;
	}
	render() {
		// this.props.url.query.slug

		return (
			// <p className="text-center mx-auto my-4 capitalize text-sm w-1/2 text-gray-600"><a href="/tienda">Tienda</a> / <a href="#">Blusas</a></p>
			<Layout>
				<PageTitle title="Blusas" />
				<ListItems />
			</Layout>

		);
	}
};
