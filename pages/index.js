import '../assets/tailwind.scss';

import Layout from '../components/Layout';
import PageTitle from '../components/PageTitle';
import ListProducts from '../components/ListProducts';
import Carousel from '../components/Carousel';


export default () => {
	return (
		<Layout>
			<div className="text-center py-4 cursor-pointer">
				<div className="p-2 bg-yellow-600 items-center text-white leading-none flex justify-center mx-auto" role="alert">
					<span className="flex rounded-full bg-red-600 uppercase px-2 py-1 text-xs font-bold mr-3 hidden">★</span>
					<span className="mr-2 text-left uppercase max1200">nueva colección</span>
					<svg className="fill-current opacity-75 h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M12.95 10.707l.707-.707L8 4.343 6.586 5.757 10.828 10l-4.242 4.243L8 15.657l4.95-4.95z"/></svg>
				</div>
			</div>
			<Carousel/>
			<PageTitle title="Productos destacados" />
			<ListProducts />
		</Layout>
	);
};

