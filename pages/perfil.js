import '../assets/tailwind.scss';
import Link from 'next/link';
import Layout from '../components/Layout';
import Router from 'next/router'

const show = () => {
     if (document.getElementById('options').className.includes('hidden')) {
          document.getElementById('options').classList.remove('hidden');
          document.getElementById('profileOptions').classList.add('bg-gray-200');
     } else {
          document.getElementById('options').classList.add('hidden');
          document.getElementById('profileOptions').classList.remove('bg-gray-200');
     }
}

export default class perfil extends React.Component {


     cerrarSesion = () => {
          localStorage.removeItem('sf');
          Router.push('/');
     }

     render() {
          console.log(this.props.url);
          return (
               <Layout>
                    <div className=" flex flex-col items-center mx-12 py-4 px-2 mt-4 md:max-w-lg md:mx-auto">
                         <img className="h-24 w-24 rounded-full shadow-xl" src="https://randomuser.me/api/portraits/women/17.jpg" alt="Perfil Nombre Completo" />
                         <p className="uppercase mt-4">{`${this.props}`}</p>
                         <div className="mt-12 w-full">
                              <div className="capitalize mt-4 p-4 border-2 border-gray-200 hover:bg-gray-200 flex justify-between">
                                   <span>Historial de Compras</span>
                              </div>
                              <div className="capitalize mt-4 p-4 border-2 border-gray-200 hover:bg-gray-200 flex justify-between">
                                   <span>Lista de Deseos</span>
                              </div>
                              <div className="mt-4 p-4 border-2 border-gray-200 hover:bg-gray-200 flex flex-col" id="profileOptions" onClick={show}>
                                   <div className="flex justify-between">
                                        <span>Ajustes de la Cuenta</span>
                                        <svg className="h-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129"><path d="M121.3 34.6c-1.6-1.6-4.2-1.6-5.8 0l-51 51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8 0-1.6 1.6-1.6 4.2 0 5.8l53.9 53.9c.8.8 1.8 1.2 2.9 1.2 1 0 2.1-.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2.1-5.8z" /></svg>
                                   </div>
                              </div>
                              <div className="hidden flex flex-col pt-2 bg-gray-200" id="options">
                                   <span className="py-4 mx-4">Cambiar Email</span>
                                   <span className="py-4 mx-4">Cambiar Contraseña</span>
                              </div>
                              <div className="capitalize mt-4 p-4 bg-red-400 text-white hover:bg-red-200 hover:text-black flex justify-between" onClick={this.cerrarSesion}>
                                   <span>Cerrar Sesión</span>
                              </div>
                         </div>
                    </div>
               </Layout>
          )
     }
}