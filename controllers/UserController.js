const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const UserModel = require('../models/UserModel');
const dotenv = require('dotenv').config();


module.exports = {
     create,
     login
};

async function create(req, res) {
     let data = req.body;
     let User = new UserModel({
          nombre: data.nombre,
          apellido: data.apellido,
          email: data.email,
          password: data.password
     });
     let hash = await bcrypt.hashSync(User.password, 10);
     User.password = hash;
     User.save()
          .then(user => {
               console.log('voy a firmar');
               const token = jwt.sign({id: user._id,nombre:user.nombre,apellido:user.apellido,email:user.email}, process.env.KEY_JWT, { expiresIn: '1h' });
               console.log('firmado',token);
               res.json({ success: true, result: token });
          })
          .catch(err => {
               console.log('error',err);
               res.json({ success: false, result: err })
          });
}

async function login(req, res) {
     let data = req.body;
     let User = new UserModel({
          email: data.email,
          password: data.password
     });
     UserModel.findOne({ email: User.email }, (err, user) => {
          if(user==null){
               res.json({ success: false, result: 'Usuario no registrado' });
          }else{
               if (bcrypt.compare(user.password, User.password)) {
                    const token = jwt.sign({id: user._id,nombre:user.nombre,apellido:user.apellido,email:user.email,admin:user.admin}, process.env.KEY_JWT, { expiresIn: '1h' });
                    res.json({ success: true, result: token });
               }
          }
     });
}